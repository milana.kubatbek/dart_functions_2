void main(List<String> arguments) {
  //Дополнительное домашнее задание по функциям:
//1 - Дан массив с числами. Проверьте, есть ли в нем два одинаковых числа не подряд.
//Если есть - выведите 'да', а если нет - выведите 'нет'.

  List<int> mylist = [1, 2, 1, 3, 2];
  int myfunc = 0;
  bool isContains = false;
  mylist.sort((a, b) => a.compareTo(b));

  mylist.forEach((e) {
    if (myfunc == e) {
      isContains = true;
    }
    myfunc = e;
  });

  if (isContains) {
    print('да');
  } else {
    print('нет');
  }

//2 - Дан массив с числами - List<int> list = [1, 2, 3, 4, 0, 5, 5, 7];
//Напишите функцию и передайте ей массив в качестве параметра.
//Функция должна вернуть вам среднее арифметическое данного массива, вы должны получить - 3.375.
  List<int> list = [1, 2, 3, 4, 0, 5, 5, 7];
  int func = 0;

  list.forEach((e) {
    func += e;
  });
  print(func / list.length);

//3 - Дан массив с числами - List<int> list = [1, 5, 3, 2, 2, 5, 6, 1, 2, 3];.
//Напишите функцию, которая вернет вам длинну этого массива - вы должны получить 10.
//Использовать lenght ЗАПРЕЩАЕТСЯ.
  List<int> listTwo = [1, 5, 3, 2, 2, 5, 6, 1, 2, 3];
  int k = 0;

  listTwo.forEach((e) {
    k = k + 1;
  });
  print(k);

//4 - Дан массив со значениями - List<dynamic> list = [1, '2', true, 1, 4, false, 'qwerty'];.
//Напишите функцию, которая будет определять тип данных каждого элемента и закидывать значение в другой массив.
//Вы должны получить - [int, String, bool, int, int, bool, String].

  List<dynamic> listType = [1, '2', true, 1, 4, false, 'qwerty'];
  List<Type> types = [];

  listType.forEach((element) {
    types.add(element.runtimeType);
  });
  print(types);
}
